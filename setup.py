import os
from setuptools import setup, find_packages

def read(fname):
	return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
	name="Google Drive Encrypted Backup System",
	version = "0.0.1",
	author = "Harro",
	author_email="harro.forge@protonmail.com",
	description= ("Utility to make encrypted backups to a google drive account"),
	packages=['gdrive_enc_bckp', "test"],
	long_description=read('README.md'),
   package_dir={'gdrive_enc_bckp':'src'},
	entry_points = {
		'console_scripts':
		[
			'gdrive-enc-bckp = gdrive_enc_bckp:main'
		]
	},

	install_required = [
		'google-api-python-client'
	],

	include_package_data=True,
	package_data= {
		'': ["README.md"]
	}
)