# "Easy" setup

1. Go to [this link](https://console.cloud.google.com/projectselector2/iam-admin/serviceaccounts)

2. Create a new project

3. Click _Create Service Account_

4. Fill the required fields and click _Create and Continue_

5. In the _Grant This Service Account Access to Project_ tab, select the role as _Admin_ and click _Continue_

6. Click _Done_

7. Click on the created account and go to the _keys_ tab

8. Click _Add Key_, select it as json file and download the json.

9. Save

10. Go to [this link](https://console.cloud.google.com/apis/library) and search and enable Google Drive API.

https://pythonhosted.org/PyDrive/quickstart.html#authentication

add drive scopes

credentials -> new OAuth client ID -> Application type web app
